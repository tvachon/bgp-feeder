#!/bin/bash

[[ $EUID != 0 ]] && echo "Must be run with sudo" && exit 1


#Parse command line flags
#If an option should be followed by an argument, it should be followed by a ":".
#Notice there is no ":" after "h". The leading ":" suppresses error messages from
#getopts. This is required to get my unrecognized option code to work.

#Set Script Name variable
SCRIPT=$(basename "${BASH_SOURCE[0]}")

#Set fonts for Help.
NORM=$(tput sgr0)
BOLD=$(tput bold)
REV=$(tput smso)

#Help function
function HELP {
  echo -e \\n"Help documentation for ${BOLD}${SCRIPT}.${NORM}"\\n
  echo "Command line switches are manditory. The following switches are recognized."
  echo "${REV}-a${NORM}  --Sets the Local ASN ${BOLD}a${NORM}"
  echo "${REV}-n${NORM}  --Sets the Source IP to use (must exist on the host)"
  echo "${REV}-d${NORM}  --Sets the Peer IP to peer with and send the tables"
  echo "${REV}-p${NORM}  --Sets the Peer's ASN (eBGP and iBGP both available)"
  echo -e "${REV}-h${NORM}  --Displays this help message. No further functions are performed."\\n
  echo -e "Example: ${BOLD}$SCRIPT -a 65000 -n 1.2.3.4 -d 4.5.6.7 -p 65001${NORM}"\\n
  exit 1
}

while getopts :a:n:d:p:h FLAG; do
  case $FLAG in
    a)
      LOCAL_ASN=$OPTARG
      ;;
    n)
      SRC_IP=$OPTARG
      ;;
    d)
      PEER_IP=$OPTARG
      ;;
    p)
      PEER_AS=$OPTARG
      ;;
    h)  #show help
      HELP
      ;;
    \?) #unrecognized option - show help
      echo -e \\n"Option -${BOLD}$OPTARG${NORM} not allowed."
      HELP
      ;;
  esac
done

shift $((OPTIND-1))  #This tells getopts to move on to the next argument.

if [[ -z $LOCAL_ASN || -z $SRC_IP || -z $PEER_AS || -z $PEER_IP ]]; then
  echo "Manditory variables are not defined"
  echo "Local ASN: ${LOCAL_ASN}"
  echo "Source IP: ${SRC_IP}"
  echo "Peer AS: ${PEER_AS}"
  echo "Peer IP: ${PEER_IP}"
  exit $LINENO
fi


#Make Tempdir
TMP_DIR_TMPL='/tmp/bgpfeed.XXXXXX'
TMPDIR=$(mktemp -d $TMP_DIR_TMPL) || exit 1

#Download latest RIPE tables
echo "Downloading latest full RIPE BGP View File"
wget --quiet -P "$TMPDIR" http://data.ris.ripe.net/rrc00/latest-bview.gz

#Expand and reformat bview
echo "Converting from BView to acceptable format for bgp_simple"
gunzip -c "$TMPDIR/latest-bview.gz" | lib/libbgpdump/bgpdump -m - > "$TMPDIR/routes_file"


carton exec ./lib/bgp_simple.pl \
  -myas $LOCAL_ASN \
  -myip $SRC_IP \
  -peerip $PEER_IP \
  -peeras $PEER_AS \
  -p "$TMPDIR/routes_file" \
  -dry
  -n 2>&1
